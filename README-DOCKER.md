# Docker for dev environment

## TL;DR

```bash
$ git clone https://gitlab.com/lkcamp/lkcamp_docs.git

$ cd lkcamp_docs

$ docker build -t local/lkcamp_docs .

$ docker run --rm -ti --name dev_lkcamp_docs -v "$(pwd)":/mkdocs -p 8000:8000 local/lkcamp_docs
```

## Building

1. Clone this repo.

2. Install [docker-ce](https://docs.docker.com/install/).

3. Run the command:

```bash
$ docker build --tag local/lkcamp_docs .
```

4. Now you have maked the docker image `local/lkcamp_docs` for dev environment.

## Usage

1. To usage this image you have maked, run the command above:

```bash
$ docker run --rm                                         \
             -tty                                         \
             --interactive                                \
             --name dev_lkcamp_docs                       \
             --volume "$(pwd)":/mkdocs                    \
             --publish 8000:8000                          \
             local/lkcamp_docs
```

2. Now open your browser and access the URL `http://localhost:8000`.
